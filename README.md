# Language Assistant

![logo](data/icons/hicolor/scalable/apps/se.jadahl.LanguageAssistant.svg)

A language assistant tool that aims to work as a smart dictionary. It's not an
translation tool. It's not tied to a specific language, but currently only
supports Chinese.

## Dictionaries

### CC-CEDICT

Language Assistant bundles [CC-CEDICT](https://cc-cedict.org/wiki/), licensed
under [CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0/).

## Screenshot

![screenshot](data/screenshot.png)

## License

Language Assistant itself is licensed GNU GPLv3+, see each dictionary for their
corresponding licenses.
