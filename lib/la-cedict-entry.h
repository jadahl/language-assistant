/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#include "la-pinyin.h"

#define LA_TYPE_CEDICT_ENTRY (la_cedict_entry_get_type ())
G_DECLARE_FINAL_TYPE (LaCedictEntry, la_cedict_entry,
                      LA, CEDICT_ENTRY, GObject)

/**
 * la_cedict_entry_get_traditional:
 *
 * Returns: (transfer none): A string with traditional characters
 */
const char * la_cedict_entry_get_traditional (LaCedictEntry *cedict_entry);

/**
 * la_cedict_entry_get_simplified:
 *
 * Returns: (transfer none): A string with simplified characters
 */
const char * la_cedict_entry_get_simplified (LaCedictEntry *cedict_entry);

/**
 * la_cedict_entry_get_pinyins:
 *
 * Returns: (transfer full) (element-type GObject): A %GPtrArray with LaPinyin
 * objects
 */
GPtrArray * la_cedict_entry_get_pinyins (LaCedictEntry *cedict_entry);

/**
 * la_cedict_entry_get_default_pinyin::
 *
 * Returns: (transfer none): A %LaPinyin
 */
LaPinyin * la_cedict_entry_get_default_pinyin (LaCedictEntry *cedict_entry);

/**
 * la_cedict_entry_get_pinyin_from_variant:
 * @variant: The variant
 *
 * Returns: (transfer none) (nullable): A %LaPinyin or %NULL.
 */
LaPinyin * la_cedict_entry_get_pinyin_from_variant (LaCedictEntry *cedict_entry,
                                                    const char    *variant);

/**
 * la_cedict_entry_take_pinyin_variant: (skip)
 */
void la_cedict_entry_take_pinyin_variant (LaCedictEntry *cedict_entry,
                                          LaPinyin      *pinyin);
/**
 * la_cedict_entry_get_definitions:
 *
 * Returns: (transfer full): A %GStrv with definitions
 */
GStrv la_cedict_entry_get_definitions (LaCedictEntry *cedict_entry);
