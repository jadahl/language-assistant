/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "la-cedict-entry.h"

#include "la-pinyin.h"

enum
{
  PROP_0,

  PROP_TRADITIONAL,
  PROP_SIMPLIFIED,
  PROP_PINYINS,
  PROP_DEFINITIONS,

  N_PROPS
};

static GParamSpec *obj_props[N_PROPS];

struct _LaCedictEntry
{
  GObject parent;

  char *traditional;
  char *simplified;

  GPtrArray *pinyins;
  GStrv definitions;
};

G_DEFINE_TYPE (LaCedictEntry, la_cedict_entry, G_TYPE_OBJECT)

static void
la_cedict_entry_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  LaCedictEntry *cedict_entry = LA_CEDICT_ENTRY (object);

  switch (prop_id)
    {
    case PROP_TRADITIONAL:
      cedict_entry->traditional = g_value_dup_string (value);
      break;
    case PROP_SIMPLIFIED:
      cedict_entry->simplified = g_value_dup_string (value);
      break;
    case PROP_PINYINS:
      cedict_entry->pinyins = g_value_dup_boxed (value);
      break;
    case PROP_DEFINITIONS:
      cedict_entry->definitions = g_value_dup_boxed (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
la_cedict_entry_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  LaCedictEntry *cedict_entry = LA_CEDICT_ENTRY (object);

  switch (prop_id)
    {
    case PROP_TRADITIONAL:
      g_value_set_string (value, cedict_entry->traditional);
      break;
    case PROP_SIMPLIFIED:
      g_value_set_string (value, cedict_entry->simplified);
      break;
    case PROP_PINYINS:
      g_value_set_boxed (value, cedict_entry->pinyins);
      break;
    case PROP_DEFINITIONS:
      g_value_set_boxed (value, cedict_entry->definitions);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
la_cedict_entry_finalize (GObject *object)
{
  LaCedictEntry *cedict_entry = LA_CEDICT_ENTRY (object);

  g_clear_pointer (&cedict_entry->traditional, g_free);
  g_clear_pointer (&cedict_entry->simplified, g_free);
  g_clear_pointer (&cedict_entry->pinyins, g_ptr_array_unref);
  g_clear_pointer (&cedict_entry->definitions, g_strfreev);

  G_OBJECT_CLASS (la_cedict_entry_parent_class)->finalize (object);
}

static void
la_cedict_entry_class_init (LaCedictEntryClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = la_cedict_entry_finalize;
  object_class->set_property = la_cedict_entry_set_property;
  object_class->get_property = la_cedict_entry_get_property;

  obj_props[PROP_TRADITIONAL] =
    g_param_spec_string ("traditional",
                         "traditional",
                         "Traditional Chinese",
                         NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);
  obj_props[PROP_SIMPLIFIED] =
    g_param_spec_string ("simplified",
                         "simplified",
                         "Simplified Chinese",
                         NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);

  /**
   * LaCedictEntry:pinyins: (type GPtrArray(GObject))
   */
  obj_props[PROP_PINYINS] =
    g_param_spec_boxed ("pinyins",
                        "pinyins",
                        "Pinyins",
                        G_TYPE_PTR_ARRAY,
                        G_PARAM_READWRITE |
                        G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS);
  obj_props[PROP_DEFINITIONS] =
    g_param_spec_boxed ("definitions",
                        "definitions",
                        "Definitions",
                        G_TYPE_STRV,
                        G_PARAM_READWRITE |
                        G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
la_cedict_entry_init (LaCedictEntry *cedict_entry)
{
}

const char *
la_cedict_entry_get_traditional (LaCedictEntry *cedict_entry)
{
  return cedict_entry->traditional;
}

const char *
la_cedict_entry_get_simplified (LaCedictEntry *cedict_entry)
{
  return cedict_entry->simplified;
}

GPtrArray *
la_cedict_entry_get_pinyins (LaCedictEntry *cedict_entry)
{
  return g_ptr_array_copy (cedict_entry->pinyins,
                           (GCopyFunc) g_object_ref, NULL);
}

LaPinyin *
la_cedict_entry_get_default_pinyin (LaCedictEntry *cedict_entry)
{
  return g_ptr_array_index (cedict_entry->pinyins, 0);
}

LaPinyin *
la_cedict_entry_get_pinyin_from_variant (LaCedictEntry *cedict_entry,
                                         const char    *variant)
{
  unsigned int i;

  for (i = 0; i < cedict_entry->pinyins->len; i++)
    {
      LaPinyin *pinyin = g_ptr_array_index (cedict_entry->pinyins, i);

      if (g_strcmp0 (variant, la_pinyin_get_variant (pinyin)) == 0)
        return pinyin;
    }

  return NULL;
}

void
la_cedict_entry_take_pinyin_variant (LaCedictEntry *cedict_entry,
                                     LaPinyin      *pinyin)
{
  g_ptr_array_add (cedict_entry->pinyins, pinyin);
}

GStrv
la_cedict_entry_get_definitions (LaCedictEntry *cedict_entry)
{
  return g_strdupv (cedict_entry->definitions);
}
