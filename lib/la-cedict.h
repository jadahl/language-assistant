/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>

typedef enum _LaCedictSearchRule
{
  LA_CEDICT_SEARCH_RULE_NONE = 0,
  LA_CEDICT_SEARCH_RULE_BEGIN = 1 << 0,
  LA_CEDICT_SEARCH_RULE_END = 1 << 1,
  LA_CEDICT_SEARCH_RULE_BOTH =
    (LA_CEDICT_SEARCH_RULE_BEGIN |
     LA_CEDICT_SEARCH_RULE_END),
} LaCedictSearchRule;

typedef enum _LaCedictSearchField
{
  LA_CEDICT_SEARCH_FIELD_NONE = 0,
  LA_CEDICT_SEARCH_FIELD_TRADITIONAL = 1 << 0,
  LA_CEDICT_SEARCH_FIELD_SIMPLIFIED = 1 << 1,
  LA_CEDICT_SEARCH_FIELD_PINYIN = 1 << 2,
  LA_CEDICT_SEARCH_FIELD_DEFINITION = 1 << 3,
} LaCedictSearchField;

#define LA_TYPE_CEDICT (la_cedict_get_type ())
G_DECLARE_FINAL_TYPE (LaCedict, la_cedict, LA, CEDICT, GObject)

/**
 * la_cedict_parse_cedict_async:
 * @cedict: A CeDict object instance
 * @uri: An URI to the dictionary
 * @cancellable: A GCancellable
 * @callback: A callback when the dictionary is parsed
 * @user_data: Pointer to pass to the callback
 */
void
la_cedict_parse_cedict_async (LaCedict            *cedict,
                              const char          *uri,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data);

/**
 * la_cedict_parse_cedict_finish:
 * @cedict: A CeDict object instance
 * @res: An async result
 * @error: (nullable): A pointer where to store an error
 *
 * Returns: %TRUE if succeeded, otherwise %FALSE
 */
gboolean
la_cedict_parse_cedict_finish (LaCedict      *cedict,
                               GAsyncResult  *res,
                               GError       **error);

/**
 * la_cedict_find_async:
 * @cedict: A CeDict object instance
 * @text: A a string to search for
 * @rules: Search rules
 * @fields: Fields to search in
 * @cancellable: A GCancellable
 * @callback: A callback when the dictionary is parsed
 * @user_data: Pointer to pass to the callback
 */
void
la_cedict_search_async (LaCedict            *cedict,
                        const char          *text,
                        LaCedictSearchRule   rules,
                        LaCedictSearchField  fields,
                        GCancellable        *cancellable,
                        GAsyncReadyCallback  callback,
                        gpointer             user_data);

/**
 * la_cedict_search_finish:
 * @cedict: A CeDict object instance
 * @res: An async result
 * @error: (nullable): A pointer where to store an error
 *
 * Returns: (transfer container) (element-type LibLa.CedictEntry): A list of
 * search results
 */
GPtrArray *
la_cedict_search_finish (LaCedict      *cedict,
                         GAsyncResult  *res,
                         GError       **error);
