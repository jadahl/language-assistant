/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "la-cedict.h"

#include <glib.h>
#include <gio/gio.h>

#include "la-cedict-entry.h"
#include "la-pinyin.h"

typedef struct _LaSearch
{
  char *text;
  LaCedictSearchRule rules;
  LaCedictSearchField fields;
} LaSearch;

struct _LaCedict
{
  GObject parent;

  GRegex *alt_pinyin_regex;
  GPtrArray *entries;
  GHashTable *traditional_to_entries;
  GHashTable *simplified_to_entries;
  GHashTable *pinyin_to_entries;
  GHashTable *definition_to_entries;
};

G_DEFINE_TYPE (LaCedict, la_cedict, G_TYPE_OBJECT)

static LaSearch *
la_search_new (const char          *text,
               LaCedictSearchRule   rules,
               LaCedictSearchField  fields)
{
  LaSearch *search;

  search = g_new0 (LaSearch, 1);
  search->text = g_strdup (text);
  search->rules = rules;
  search->fields = fields;

  return search;
}

static void
la_search_free (LaSearch *search)
{
  g_free (search->text);
  g_free (search);
}

static void
la_cedict_finalize (GObject *object)
{
  LaCedict *cedict = LA_CEDICT (object);

  g_clear_pointer (&cedict->alt_pinyin_regex, g_regex_unref);
  g_clear_pointer (&cedict->entries, g_ptr_array_unref);
  g_clear_pointer (&cedict->traditional_to_entries, g_hash_table_unref);
  g_clear_pointer (&cedict->simplified_to_entries, g_hash_table_unref);
  g_clear_pointer (&cedict->pinyin_to_entries, g_hash_table_unref);
  g_clear_pointer (&cedict->definition_to_entries, g_hash_table_unref);

  G_OBJECT_CLASS (la_cedict_parent_class)->finalize (object);
}

static void
la_cedict_class_init (LaCedictClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = la_cedict_finalize;
}

static void
la_cedict_init (LaCedict *cedict)
{
  cedict->entries = g_ptr_array_new ();
  g_ptr_array_set_free_func (cedict->entries, g_object_unref);

  cedict->traditional_to_entries =
     g_hash_table_new_full (g_str_hash,
                            g_str_equal,
                            g_free,
                            (GDestroyNotify) g_ptr_array_unref);
  cedict->simplified_to_entries =
    g_hash_table_new_full (g_str_hash,
                           g_str_equal,
                           g_free,
                           (GDestroyNotify) g_ptr_array_unref);
  cedict->pinyin_to_entries =
    g_hash_table_new_full (g_str_hash,
                           g_str_equal,
                           g_free,
                           (GDestroyNotify) g_ptr_array_unref);
  cedict->definition_to_entries =
    g_hash_table_new_full (g_str_hash,
                           g_str_equal,
                           g_free,
                           (GDestroyNotify) g_ptr_array_unref);
}


static gboolean
consume_byte (GDataInputStream  *data_stream,
              char               expected_byte,
              GError           **error)
{
  char read_byte;

  read_byte = g_data_input_stream_read_byte (data_stream, NULL, error);
  if (*error)
    return FALSE;

  if (read_byte != expected_byte)
    {
      g_set_error (error, G_IO_ERROR, G_IO_ERROR_FAILED,
                   "Expected '%c' but got '%c'",
                   expected_byte, read_byte);
      return FALSE;
    }

  return TRUE;
}

static LaPinyin *
parse_alternative_pinyin (LaCedict *cedict,
                          char     *metadata)
{
  g_autoptr (GMatchInfo) match_info = NULL;

  if (!cedict->alt_pinyin_regex)
    {
      GError *error = NULL;

      cedict->alt_pinyin_regex =
        g_regex_new ("^(Taiwan|Beijing|also) pr. \\[([^\\]]+)\\]$",
                     0, 0, &error);
      if (!cedict->alt_pinyin_regex)
        g_error ("Failed to compile regex: %s", error->message);
    }

  if (g_regex_match (cedict->alt_pinyin_regex, metadata, 0, &match_info))
    {
      char *variant;
      char *pinyin;

      variant = g_match_info_fetch (match_info, 1);
      pinyin = g_match_info_fetch (match_info, 2);

      return g_object_new (LA_TYPE_PINYIN,
                           "variant", variant,
                           "pinyin", pinyin,
                           NULL);
    }

  return NULL;
}

static GPtrArray *
ensure_entries (GHashTable *table,
                const char *key)
{
  GPtrArray *entries;

  entries = g_hash_table_lookup (table, key);
  if (!entries)
    {
      entries = g_ptr_array_new ();
      g_hash_table_insert (table, g_strdup (key), entries);
    }
  return entries;
}

static gboolean
parse_line (LaCedict  *cedict,
            char      *line,
            GError   **error)
{
  g_autoptr (GInputStream) input_stream = NULL;
  g_autoptr (GDataInputStream) data_stream = NULL;
  g_autofree char *traditional = NULL;
  g_autofree char *simplified = NULL;
  g_autofree char *pinyin = NULL;
  g_autoptr (GPtrArray) pinyins = NULL;
  g_autoptr (GStrvBuilder) definitions_builder = NULL;
  g_auto (GStrv) definitions = NULL;
  GPtrArray *entries;
  LaCedictEntry *entry;
  unsigned int i;

  g_return_val_if_fail (error, FALSE);

  g_strstrip (line);
  if (strlen (line) == 0)
    return TRUE;

  if (line[0] == '#')
    return TRUE;

  input_stream = g_memory_input_stream_new_from_data (line,
                                                      strlen (line),
                                                      NULL);
  data_stream = g_data_input_stream_new (input_stream);

  /* First word are the traditional characters. */
  traditional = g_data_input_stream_read_upto (data_stream,  " ", 1,
                                               NULL, NULL, error);
  if (!traditional)
    return FALSE;

  if (!consume_byte (data_stream, ' ', error))
    return FALSE;

  /* The second word are the simplified characters. */
  simplified = g_data_input_stream_read_upto (data_stream, " ", 1,
                                              NULL, NULL, error);
  if (!simplified)
    return FALSE;

  if (!consume_byte (data_stream, ' ', error))
    return FALSE;

  /* The pronounciation is between [ and ] */
  if (!consume_byte (data_stream, '[', error))
    return FALSE;

  pinyin = g_data_input_stream_read_upto (data_stream, "]", 1,
                                          NULL, NULL, error);
  if (!pinyin)
    return FALSE;

  pinyins = g_ptr_array_new ();
  g_ptr_array_add (pinyins, g_object_new (LA_TYPE_PINYIN,
                                          "variant", "default",
                                          "pinyin", pinyin,
                                          NULL));

  if (!consume_byte (data_stream, ']', error))
    return FALSE;
  if (!consume_byte (data_stream, ' ', error))
    return FALSE;

  /* Lastly, the meaning, as well as alt. pronounciations, surrounded and
   * grouped by '/':
   *
   * /hello/hi/
   */

  if (!consume_byte (data_stream, '/', error))
    return FALSE;

  definitions_builder = g_strv_builder_new ();

  while (TRUE)
    {
      g_autofree char *metadata = NULL;
      g_autoptr (LaPinyin) alt_pinyin = NULL;

      metadata = g_data_input_stream_read_upto (data_stream, "/", 1,
                                                NULL, NULL, error);
      if (*error)
        return FALSE;
      if (!metadata)
        break;

      alt_pinyin = parse_alternative_pinyin (cedict, metadata);
      if (alt_pinyin)
        g_ptr_array_add (pinyins, g_object_ref (alt_pinyin));
      else
        g_strv_builder_add (definitions_builder, metadata);

      if (!consume_byte (data_stream, '/', error))
        return FALSE;
    }

  definitions = g_strv_builder_end (definitions_builder);
  entry = g_object_new (LA_TYPE_CEDICT_ENTRY,
                        "traditional", traditional,
                        "simplified", simplified,
                        "pinyins", pinyins,
                        "definitions", definitions,
                        NULL);
  g_ptr_array_add (cedict->entries, entry);

  entries = ensure_entries (cedict->traditional_to_entries, traditional);
  g_ptr_array_add (entries, entry);
  entries = ensure_entries (cedict->simplified_to_entries, simplified);
  g_ptr_array_add (entries, entry);

  for (i = 0; definitions[i]; i++)
    {
      g_autoptr (GString) definition = NULL;

      definition = g_string_new (definitions[i]);
      g_string_ascii_down (definition);

      entries = ensure_entries (cedict->definition_to_entries, definition->str);
      g_ptr_array_add (entries, entry);
    }

  return TRUE;
}

static void
update_alt_pinyin (LaCedict      *cedict,
                   LaCedictEntry *entry,
                   const char    *variant)
{
  const char *traditional;
  const char *simplified;
  LaPinyin *reference_pinyin;
  g_auto (GStrv) split_reference_pinyin = NULL;
  int i;
  gboolean replaced = FALSE;

  if (la_cedict_entry_get_pinyin_from_variant (entry, variant))
    return;

  traditional = la_cedict_entry_get_traditional (entry);
  simplified = la_cedict_entry_get_simplified (entry);

  reference_pinyin = la_cedict_entry_get_default_pinyin (entry);
  split_reference_pinyin = g_strsplit (la_pinyin_get_pinyin (reference_pinyin),
                                       " ", -1);

  for (i = 0;; i++)
    {
      gunichar unichar_traditional = g_utf8_get_char (traditional);
      gunichar unichar_simplified = g_utf8_get_char (simplified);
      char character_traditional[6] = {};
      char character_simplified[6] = {};
      GPtrArray *entries;
      unsigned int j;

      traditional = g_utf8_next_char (traditional);
      simplified = g_utf8_next_char (simplified);

      if (!unichar_traditional || !unichar_simplified)
        break;

      g_unichar_to_utf8 (unichar_traditional, character_traditional);
      entries = g_hash_table_lookup (cedict->traditional_to_entries,
                                     character_traditional);
      if (!entries)
        continue;

      for (j = 0; j < entries->len; j++)
        {
          LaCedictEntry *character_entry = g_ptr_array_index (entries, j);
          LaPinyin *default_pinyin;
          LaPinyin *alt_pinyin;

          default_pinyin = la_cedict_entry_get_default_pinyin (character_entry);
          if (g_strcmp0 (la_pinyin_get_pinyin (default_pinyin),
                         split_reference_pinyin[i]) != 0)
            break;

          alt_pinyin = la_cedict_entry_get_pinyin_from_variant (character_entry,
                                                                variant);
          if (!alt_pinyin)
            continue;

          g_clear_pointer (&split_reference_pinyin[i], g_free);
          split_reference_pinyin[i] =
            g_strdup (la_pinyin_get_pinyin (alt_pinyin));
          replaced = TRUE;
        }
    }

  if (replaced)
    {
      g_autofree char *new_pinyin = NULL;

      new_pinyin = g_strjoinv (" ", split_reference_pinyin);
      la_cedict_entry_take_pinyin_variant (entry,
                                           g_object_new (LA_TYPE_PINYIN,
                                                         "variant", variant,
                                                         "pinyin", new_pinyin,
                                                         NULL));
    }
}

static void
update_alt_pinyins (LaCedict *cedict)
{
  unsigned int i;

  for (i = 0; i < cedict->entries->len; i++)
    {
      LaCedictEntry *entry = g_ptr_array_index (cedict->entries, i);
      g_autoptr (GPtrArray) pinyins = NULL;
      unsigned int j;
      const char *traditional;
      const char *simplified;

      traditional = la_cedict_entry_get_traditional (entry);
      simplified = la_cedict_entry_get_simplified (entry);
      if (g_utf8_strlen (traditional, -1) > 1)
        update_alt_pinyin (cedict, entry, "Taiwan");

      pinyins = la_cedict_entry_get_pinyins (entry);
      for (j = 0; j < pinyins->len; j++)
        {
          LaPinyin *pinyin = g_ptr_array_index (pinyins, j);
          g_autofree char *pinyin_trimmed = NULL;
          GPtrArray *entries;

          pinyin_trimmed = la_pinyin_get_trimmed (pinyin);
          entries = ensure_entries (cedict->pinyin_to_entries, pinyin_trimmed);
          g_ptr_array_add (entries, entry);
        }
    }
}

static void
parse_cedict_in_thread (GTask        *task,
                        gpointer      source_object,
                        gpointer      task_data,
                        GCancellable *cancellable)
{
  LaCedict *cedict = LA_CEDICT (source_object);
  char *uri = g_task_get_task_data (task);
  g_autoptr (GFile) file = NULL;
  g_autoptr (GFileInputStream) file_stream = NULL;
  g_autoptr (GConverter) converter = NULL;
  g_autoptr (GInputStream) converter_stream = NULL;
  g_autoptr (GDataInputStream) data_stream = NULL;
  g_autoptr (GError) error = NULL;

  file = g_file_new_for_uri (uri);
  file_stream = g_file_read (file, cancellable, &error);
  if (!file_stream)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  converter =
    G_CONVERTER (g_zlib_decompressor_new (G_ZLIB_COMPRESSOR_FORMAT_GZIP));
  converter_stream = g_converter_input_stream_new (G_INPUT_STREAM (file_stream),
                                                   converter);
  data_stream = g_data_input_stream_new (converter_stream);

  while (TRUE)
    {
      g_autofree char *line = NULL;

      line = g_data_input_stream_read_line_utf8 (data_stream,
                                                 NULL, NULL,
                                                 NULL);
      if (!line)
        {
          break;
        }

      if (!parse_line (cedict, line, &error))
        {
          g_task_return_error (task, g_steal_pointer (&error));
          return;
        }
    }

  update_alt_pinyins (cedict);

  g_task_return_boolean (task, TRUE);
}

void
la_cedict_parse_cedict_async (LaCedict            *cedict,
                              const char          *uri,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  GTask *task;

  task = g_task_new (cedict, cancellable, callback, user_data);
  g_task_set_task_data (task, g_strdup (uri), g_free);
  g_task_run_in_thread (task, parse_cedict_in_thread);
}

gboolean
la_cedict_parse_cedict_finish (LaCedict      *cedict,
                               GAsyncResult  *res,
                               GError       **error)
{
  return g_task_propagate_boolean (G_TASK (res), error);
}

static void
search_in_thread (GTask        *task,
                  gpointer      source_object,
                  gpointer      task_data,
                  GCancellable *cancellable)
{
  LaCedict *cedict = LA_CEDICT (source_object);
  LaSearch *search = g_task_get_task_data (task);
  GPtrArray *results = NULL;

  if (search->rules & LA_CEDICT_SEARCH_RULE_BEGIN &&
      search->rules & LA_CEDICT_SEARCH_RULE_END)
    {
      if (search->fields & LA_CEDICT_SEARCH_FIELD_TRADITIONAL)
        {
          results = g_hash_table_lookup (cedict->traditional_to_entries,
                                         search->text);
        }
      if (results)
        goto out_add_ref;

      if (search->fields & LA_CEDICT_SEARCH_FIELD_SIMPLIFIED)
        {
          results = g_hash_table_lookup (cedict->simplified_to_entries,
                                         search->text);
        }
      if (results)
        goto out_add_ref;

      if (search->fields & LA_CEDICT_SEARCH_FIELD_PINYIN)
        {
          results = g_hash_table_lookup (cedict->pinyin_to_entries,
                                         search->text);
        }
      if (results)
        goto out_add_ref;

      if (search->fields & LA_CEDICT_SEARCH_FIELD_DEFINITION)
        {
          results = g_hash_table_lookup (cedict->definition_to_entries,
                                         search->text);
        }
      if (results)
        goto out_add_ref;

      results = g_ptr_array_new ();
    }
  else
    {
      unsigned int i;

      results = g_ptr_array_new ();
      for (i = 0; i < cedict->entries->len; i++)
        {
          LaCedictEntry *entry = g_ptr_array_index (cedict->entries, i);
          g_auto (GStrv) texts = NULL;
          int j;

          if (search->fields & LA_CEDICT_SEARCH_FIELD_TRADITIONAL)
            {
              texts = g_new0 (char *, 2);
              texts[0] = g_strdup (la_cedict_entry_get_traditional (entry));
            }
          else if (search->fields & LA_CEDICT_SEARCH_FIELD_SIMPLIFIED)
            {
              texts = g_new0 (char *, 2);
              texts[0] = g_strdup (la_cedict_entry_get_simplified (entry));
            }
          else if (search->fields & LA_CEDICT_SEARCH_FIELD_PINYIN)
            {
              g_warn_if_reached ();
              texts = g_new0 (char *, 1);
            }
          else if (search->fields & LA_CEDICT_SEARCH_FIELD_DEFINITION)
            {
              texts = g_strdupv (la_cedict_entry_get_definitions (entry));
            }

          for (j = 0; texts[j]; j++)
            {
              char *entry_text = texts[j];
              char *result;
              gboolean found;

              result = strcasestr (entry_text, search->text);

              found = !!result;

              if (found && search->rules & LA_CEDICT_SEARCH_RULE_BEGIN)
                found &= result == entry_text;

              if (found && search->rules & LA_CEDICT_SEARCH_RULE_END)
                {
                  int result_len;

                  result_len = strlen (search->text);
                  found &= result[result_len] == '\0';
                }

              if (found)
                g_ptr_array_add (results, entry);

              if (results->len > 10)
                goto out;
            }
        }
        goto out;
      }

out_add_ref:
          g_ptr_array_ref (results);
out:
  return g_task_return_pointer (task, results,
                                (GDestroyNotify) g_ptr_array_unref);
}

void
la_cedict_search_async (LaCedict            *cedict,
                        const char          *text,
                        LaCedictSearchRule   rules,
                        LaCedictSearchField  fields,
                        GCancellable        *cancellable,
                        GAsyncReadyCallback  callback,
                        gpointer             user_data)
{
  GTask *task;
  LaSearch *search;

  search = la_search_new (text, rules, fields);

  task = g_task_new (cedict, cancellable, callback, user_data);
  g_task_set_task_data (task, search, (GDestroyNotify) la_search_free);
  g_task_run_in_thread (task, search_in_thread);
}

GPtrArray *
la_cedict_search_finish (LaCedict      *cedict,
                         GAsyncResult  *res,
                         GError       **error)
{
  return g_ptr_array_ref (g_task_propagate_pointer (G_TASK (res), error));
}

