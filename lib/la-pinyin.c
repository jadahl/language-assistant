/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "la-pinyin.h"

#include <glib.h>

enum
{
  PROP_0,

  PROP_VARIANT,
  PROP_PINYIN,

  N_PROPS
};

static GParamSpec *obj_props[N_PROPS];

struct _LaPinyin
{
  GObject parent;

  char *variant;
  char *pinyin;
};

G_DEFINE_TYPE (LaPinyin, la_pinyin, G_TYPE_OBJECT)

static void
la_pinyin_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  LaPinyin *pinyin = LA_PINYIN (object);

  switch (prop_id)
    {
    case PROP_VARIANT:
      pinyin->variant = g_value_dup_string (value);
      break;
    case PROP_PINYIN:
      pinyin->pinyin = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
la_pinyin_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  LaPinyin *pinyin = LA_PINYIN (object);

  switch (prop_id)
    {
    case PROP_VARIANT:
      g_value_set_string (value, pinyin->variant);
      break;
    case PROP_PINYIN:
      g_value_set_string (value, pinyin->pinyin);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
la_pinyin_finalize (GObject *object)
{
  LaPinyin *pinyin = LA_PINYIN (object);

  g_clear_pointer (&pinyin->variant, g_free);
  g_clear_pointer (&pinyin->pinyin, g_free);

  G_OBJECT_CLASS (la_pinyin_parent_class)->finalize (object);
}

static void
la_pinyin_class_init (LaPinyinClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = la_pinyin_finalize;
  object_class->set_property = la_pinyin_set_property;
  object_class->get_property = la_pinyin_get_property;

  obj_props[PROP_VARIANT] =
    g_param_spec_string ("variant",
                         "variant",
                         "Variant",
                         NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);
  obj_props[PROP_PINYIN] =
    g_param_spec_string ("pinyin",
                         "pinyin",
                         "Pinyin",
                         NULL,
                         G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
la_pinyin_init (LaPinyin *pinyin)
{
}

char *
la_pinyin_get_trimmed (LaPinyin *pinyin)
{
  GArray *array;
  int i, len;

  array = g_array_new (TRUE, FALSE, sizeof (char));
  len = strlen (pinyin->pinyin);
  for (i = 0; i < len; i++)
    {
      char c = pinyin->pinyin[i];

      if (g_ascii_isalpha (c))
        g_array_append_val (array, c);
    }

  return g_array_free (array, FALSE);
}

const char *
la_pinyin_get_variant (LaPinyin *pinyin)
{
  return pinyin->variant;
}

const char *
la_pinyin_get_pinyin (LaPinyin *pinyin)
{
  return pinyin->pinyin;
}
