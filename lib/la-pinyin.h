/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

#define LA_TYPE_PINYIN (la_pinyin_get_type ())
G_DECLARE_FINAL_TYPE (LaPinyin, la_pinyin, LA, PINYIN, GObject)

char * la_pinyin_get_trimmed (LaPinyin *pinyin);

const char * la_pinyin_get_variant (LaPinyin *pinyin);

const char * la_pinyin_get_pinyin (LaPinyin *pinyin);
