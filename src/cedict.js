/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { Gio, GLib, LibLa } = imports.gi;

var CedictSearchRule = LibLa.CedictSearchRule;
var CedictSearchField = LibLa.CedictSearchField;

var Cedict = class Cedict {
    constructor(readyCallback) {
        this._loadDictionary();
        this._readyCallback = readyCallback;
    }

    _loadDictionary() {
        this._cedict = new LibLa.Cedict();
        this._cedict.parse_cedict_async(
            'resource:///se/jadahl/LanguageAssistant/cc-cedict/cc-cedict.txt.gz',
            null,
            (cedict, res) => {
                let resource = cedict.parse_cedict_finish(res);
                this._readyCallback();
            });
    }

    search(text, rules, fields, cancellable=null) {
        return new Promise((resolve, reject) => {
            this._cedict.search_async(text, rules, fields, cancellable,
                (cedict, res) => {
                    try {
                        const result = cedict.search_finish(res)
                        resolve(result);
                    } catch(e) {
                        reject(e);
                    }
                });
        });
    }
};
