/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const _uColonRegex = /u:/g;
const _toneRegex = /^[12345]$/;
const _tonePriorities = [
    /([ae])/,
    /(o)u/,
    /([iouü])/,
];
const _combineCharacters = {
    '1': '\u0304',
    '2': '\u0301',
    '3': '\u030C',
    '4': '\u0300',
    '5': '',
};

// Pinyin to Bopomofo translation ported from
// https://gist.github.com/ttempe/4010474
const _bopomofoSpecialReplacements = [
    ['yu', 'u:'],     ['v', 'u:'],
    ['yi', 'i'],      ['you', 'ㄧㄡ'],  ['y', 'i'],
    ['wu', 'u'],      ['wong', 'ㄨㄥ'], ['w', 'u'],
]

const _bopomofoTable = [
    // Special cases
    ['ju',   'ㄐㄩ'], ['qu',   'ㄑㄩ'], ['xu',  'ㄒㄩ'],
    ['zhi',  'ㄓ'],   ['chi',  'ㄔ'],   ['shi', 'ㄕ'],   ['ri',   'ㄖ'],
    ['zi',   'ㄗ'],   ['ci',   'ㄘ'],   ['si',  'ㄙ'],
    ['r5',   'ㄦ'],

    // Initials
    ['b',    'ㄅ'],   ['p',    'ㄆ'],   ['m',   'ㄇ'],   ['f',    'ㄈ'],
    ['d',    'ㄉ'],   ['t',    'ㄊ'],   ['n',   'ㄋ'],   ['l',    'ㄌ'],
    ['g',    'ㄍ'],   ['k',    'ㄎ'],   ['h',   'ㄏ'],
    ['j',    'ㄐ'],   ['q',    'ㄑ'],   ['x',   'ㄒ'],
    ['zh',   'ㄓ'],   ['ch',   'ㄔ'],   ['sh',  'ㄕ'],   ['r',    'ㄖ'],
    ['z',    'ㄗ'],   ['c',    'ㄘ'],   ['s',   'ㄙ'],

    // Finals
    ['i',    'ㄧ'],   ['u',    'ㄨ'],    ['u:',  'ㄩ'],
    ['a',    'ㄚ'],   ['o',    'ㄛ'],   ['e',   'ㄜ'],   ['ê',    'ㄝ'],
    ['ai',   'ㄞ'],   ['ei',   'ㄟ'],   ['ao',  'ㄠ'],   ['o',   'ㄡ'],
    ['an',   'ㄢ'],   ['en',   'ㄣ'],   ['ang', 'ㄤ'],   ['eng',  'ㄥ'],
    ['er',   'ㄦ'],
    ['ia',   'ㄧㄚ'], ['io',   'ㄧㄛ'], ['ie',  'ㄧㄝ'], ['iai',  'ㄧㄞ'],
    ['iao',  'ㄧㄠ'], ['iu',   'ㄧㄡ'],  ['ian', 'ㄧㄢ'],
    ['in',   'ㄧㄣ'], ['iang', 'ㄧㄤ'], ['ing', 'ㄧㄥ'],
    ['ua',   'ㄨㄚ'], ['uo',   'ㄨㄛ'], ['uai', 'ㄨㄞ'],
    ['ui',   'ㄨㄟ'], ['uan',  'ㄨㄢ'], ['un',  'ㄨㄣ'],
    ['uang', 'ㄨㄤ'], ['ong',  'ㄨㄥ'],
    ['u:e',  'ㄩㄝ'], ['u:an', 'ㄩㄢ'], ['u:n', 'ㄩㄣ'], ['iong', 'ㄩㄥ'],

    // Tones
    ['1',    ''],     ['2',    'ˊ'],
    ['3',    'ˇ'],    ['4',    'ˋ'],
    ['5',    '˙'],
]
_bopomofoTable.sort((a, b) => {
    if (a[0].length == b[0].length)
        return 0;
    else if (a[0].length > b[0].length)
        return -1;
    else
        return 1;
    });
const _bopomofoReplacements =
  _bopomofoSpecialReplacements.concat(_bopomofoTable);

var PronunciationVariant = {
    HANYU_PINYIN: 1,
    BOPOMOFO: 2,
};

var Pronunciation = class Pronunciation {
    constructor(cedictPinyin) {
        this._cedictPinyin = cedictPinyin;
    }

    format(variant) {
        switch (variant) {
        case PronunciationVariant.HANYU_PINYIN:
            return this._formatHanyuPinyin();
        case PronunciationVariant.BOPOMOFO:
            return this._formatBopomofo();
        default:
            throw new Error('Invalid pronunciation variant');
        }
    }

    _formatHanyuPinyinWord(cedictPinyinWord) {
        let hanyuPinyin = cedictPinyinWord.replace(_uColonRegex,'ü');
        const lastLetter = hanyuPinyin.substr(-1);
        if (lastLetter.match(_toneRegex)) {
            for (const toneLetterRegex of _tonePriorities) {
                let toneLetterMatch = hanyuPinyin.match(toneLetterRegex);
                if (!toneLetterMatch)
                    continue;

                hanyuPinyin = hanyuPinyin.substr(0, hanyuPinyin.length - 1);
                const tone = lastLetter;

                const toneLetter = toneLetterMatch[1];
                hanyuPinyin = hanyuPinyin.replace(
                    toneLetter,
                    (toneLetter + _combineCharacters[tone]).normalize("NFC"));

                return hanyuPinyin;
            }
        } else {
            return hanyuPinyin;
        }
    }

    _formatHanyuPinyin() {
        return this._cedictPinyin.split(' ')
            .map(word => this._formatHanyuPinyinWord(word))
            .join(' ');
    }

    _formatBopomofo() {
        let string = this._cedictPinyin.toLowerCase();
        for (const replacement of _bopomofoReplacements)
            string = string.replaceAll(replacement[0], replacement[1]);

        return string;
    }
};
