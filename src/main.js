/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pkg.initGettext();
pkg.initFormat();
pkg.require({
  'Gio': '2.0',
  'Gtk': '4.0',
  'Gdk': '4.0',
  'Adw': '1',
});

const { Gio, Gtk, Gdk, Adw } = imports.gi;
const Package = imports.package;

const { LanguageAssistantWindow } = imports.window;

function main(argv) {
    const application = new Adw.Application({
        application_id: 'se.jadahl.LanguageAssistant',
        flags: Gio.ApplicationFlags.FLAGS_NONE,
    });

    application.connect('activate', app => {
        let activeWindow = app.activeWindow;

        if (!activeWindow) {
            activeWindow = new LanguageAssistantWindow(app);
        }

        activeWindow.present();
    });

    let action;
    action = new Gio.SimpleAction({ name: 'quit' });
    action.connect('activate', app => app.activeWindow.close());
    application.add_action(action);

    action = new Gio.SimpleAction({ name: 'zoom-in' });
    action.connect('activate', app => {
      application.activeWindow.zoom(1);
    });
    application.add_action(action);
    action = new Gio.SimpleAction({ name: 'zoom-out' });
    action.connect('activate', app => {
      application.activeWindow.zoom(-1);
    });
    application.add_action(action);

    action = new Gio.SimpleAction({ name: 'about' });
    action.connect('activate', app => {
        let aboutDialog = new Gtk.AboutDialog({
            authors: [
                'Jonas Ådahl <jadahl@gmail.org>',
            ],
            translator_credits: _('translator-credits'),
            program_name: _('Language Assistant'),
            comments: _('Language assistance tools'),
            license_type: Gtk.License.GPL_3_0,
            logo_icon_name: Package.name,
            version: Package.version,
            transient_for: application.activeWindow,
            modal: true,
        });
        aboutDialog.add_credit_section('CC-CEDICT', [
            'Copyright © 1997, 1998 Paul Andrew Denisowsk',
            'Copyright © 2007-2021 CC-CEDICT Authors']);
        aboutDialog.add_credit_section('Bopomofo', [
            'Copyright © 2012 Thomas TEMPÉ',
            'Copyright © 2014 Alex Griffin']);
        aboutDialog.present();
    });
    application.add_action(action);

    const provider = Gtk.CssProvider.new();
    provider.load_from_resource(
        '/se/jadahl/LanguageAssistant/stylesheet.css');

    Gtk.init();
    const display = Gdk.Display.get_default();
    Gtk.StyleContext.add_provider_for_display (
        display,
        provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    const theme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default());
    theme.add_resource_path("/se/jadahl/LanguageAsisstant/icons");

    return application.run(argv);
}
