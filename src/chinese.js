/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gio, GLib, Gtk, Gdk, Pango } = imports.gi;
const { Cedict, CedictSearchRule, CedictSearchField } = imports.cedict;

const { Language } = imports.language;
const { Pronunciation, PronunciationVariant } = imports.pronunciation;
const { PresentationMode } = imports.types;

const Variant = {
    TRADITIONAL: 0,
    SIMPLIFIED: 1,
};

const SearchMethod = {
    WORD_BY_WORD: 1,
    FREE_TEXT: 2,
};

const PronunciationRegion = {
    TAIWAN: 1,
    CHINA: 2,
};

function forwardError(error) {
    log(error);
    log(error.stack);
    throw error;
}

function countCharacters(str) {
  return [...str].length;
}

class Sentence {
    constructor(text) {
        let parts = text.trim().split(/[\.,\/. 　。【】『』「」『』\(\)\[\]\{\}]/);
        this.parts = [];
        for (let part of parts) {
            part = part.trim();
            if (part.length > 0)
                this.parts.push(part);
        }
    }

    isEmpty() {
        return this.parts.length === 0;
    }

    peekFirstPart() {
        return this.parts[0];
    }
};

var ChineseResultEntry = GObject.registerClass({
    GTypeName: 'ChineseResultEntry',
    Template: 'resource:///se/jadahl/LanguageAssistant/chinese.ui',
    InternalChildren: [
        'word',
        'pinyin',
        'definitions',
        'prev_definition',
        'next_definition',
    ]
}, class ChineseResultEntry extends Gtk.Box {
    _init(word, pronunciation, definitions, needsHeader) {
        super._init({
          vexpand: true,
          valign: Gtk.Align.FILL,
        });

        this._word.label = word;
        this._needsHeader = needsHeader;

        const pinyinText =
            pronunciation.format(PronunciationVariant.HANYU_PINYIN);
        const bopomofoText =
            pronunciation.format(PronunciationVariant.BOPOMOFO);
        this._pronunciation = pronunciation;
        this._pinyin.label = pinyinText;
        this._pinyin.set_tooltip_markup(
            _('Hanyu pinyin: ') + '<b>' + pinyinText + '</b>\n' +
            _('Bopomofo: ') + '<b>' + bopomofoText + '</b>');

        for (let definition of definitions) {
            const labelText = this._filterPronunciations(definition);
            const label = new Gtk.Label({
                label: labelText,
                tooltip_text: labelText,
                ellipsize: Pango.EllipsizeMode.END,
                has_tooltip: true,
                halign: Gtk.Align.START,
                hexpand: true,
                css_classes: ['chinese-definition'],
            });

            this._definitions.append(label);
        }
        this._definitions.connect('page-changed',
            this._syncCarouselArrows.bind(this));
        this._prev_definition.connect('clicked',
            this._prevDefinition.bind(this));
        this._next_definition.connect('clicked',
            this._nextDefinition.bind(this));
        this._syncCarouselArrows();

        this._contextMenus = [
            {
              widget: this._word,
              menu: this._openContextMenu.bind(this),
            },
            {
              widget: this._definitions,
              menu: () => {
                  const currentPage =
                      this._definitions.get_nth_page(
                          Math.round(this._definitions.get_position()));
                  this._openContextMenu(currentPage);
              },
            },
            {
              widget: this._pinyin,
              menu: this._openPronunciationContextMenu.bind(this),
            },
        ];
        this._addContextMenu();
    }

    setPresentationMode(presentationMode) {
        switch (presentationMode) {
            case PresentationMode.LIST:
                return;
            case PresentationMode.WORD_BY_WORD:
                for (let i = 0; i < this._definitions.get_n_pages(); i++)
                    this._definitions.get_nth_page(i).set_max_width_chars(20);
                break;
        }
    }

    _syncCarouselArrows() {
        const nPages = this._definitions.get_n_pages();
        if (nPages === 1) {
            this._prev_definition.hide();
            this._next_definition.hide();
        } else {
            const nthPage = Math.round(this._definitions.get_position());
            if (nthPage === 0) {
                this._prev_definition.set_sensitive(false);
                this._next_definition.set_sensitive(true);
            } else if (nthPage === nPages - 1) {
                this._prev_definition.set_sensitive(true);
                this._next_definition.set_sensitive(false);
            } else {
                this._prev_definition.set_sensitive(true);
                this._next_definition.set_sensitive(true);
            }
        }
    }

    _prevDefinition() {
        const position = Math.round(this._definitions.get_position());
        if (position === 0)
            return;

        const nextWidget = this._definitions.get_nth_page(position - 1);
        this._definitions.scroll_to(nextWidget, true);
    }

    _nextDefinition() {
        const position = Math.round(this._definitions.get_position());
        if (position === this._definitions.get_n_pages() - 1)
            return;

        const nextWidget = this._definitions.get_nth_page(position + 1);
        this._definitions.scroll_to(nextWidget, true);
    }

    _filterPronunciations(definition) {
        const re = /\[[a-zA-Z1-5 ]+\]/g;
        const match = definition.match(re);
        if (match) {
            const pinyins = [...match];
            for (let pinyin of pinyins) {
                pinyin = pinyin.slice(1, -1);
                const pronunciation = new Pronunciation(pinyin);
                definition = definition.replace(`[${pinyin}]`,
                    pronunciation.format(PronunciationVariant.HANYU_PINYIN));
            }
        }
        return definition;
    }

    _findMenu(pickedWidget) {
        for (let menu of this._contextMenus) {
            if (pickedWidget === menu.widget ||
                pickedWidget.is_ancestor(menu.widget)) {
                return menu
            }
        }
        return null;
    }

    _addContextMenu() {
        const longPressGesture = Gtk.GestureLongPress.new();
        const contextMenuFunc = (widget) => {
            const menu = this._findMenu(widget);
            if (!menu)
                return;

            menu.menu(menu.widget);
        }
        longPressGesture.connect('pressed',
            (_, x, y) => {
                const pickedWidget = this.pick(x, y, Gtk.PickFlags.NON_TARGETABLE);
                contextMenuFunc(pickedWidget);
            });
        this.add_controller(longPressGesture);
        const rightClickGesture = new Gtk.GestureClick({
            button: Gdk.BUTTON_SECONDARY,
        });
        rightClickGesture.connect('pressed',
            (_, _nPress, x, y) => {
                const pickedWidget = this.pick(x, y, Gtk.PickFlags.NON_TARGETABLE);
                contextMenuFunc(pickedWidget);
            });
        this.add_controller(rightClickGesture);
    }

    _openContextMenu(label) {
        const builder = Gtk.Builder.new_from_resource(
            '/se/jadahl/LanguageAssistant/chinese-menus.ui');
        const popover = Gtk.PopoverMenu.new_from_model(
            builder.get_object('result_context_menu'));

        const actionGroup = new Gio.SimpleActionGroup();
        const action = new Gio.SimpleAction({ name: 'copy'});

        action.connect('activate', app => {
            const display = Gdk.Display.get_default();
            const clipboard = display.get_clipboard();
            const encoder = new TextEncoder();
            const text = encoder.encode(label.label);
            const contentProvider =
                Gdk.ContentProvider.new_for_bytes(
                    'text/plain;charset=utf-8', text);
            clipboard.set_content(contentProvider);
        });
        actionGroup.insert(action);
        popover.insert_action_group('chinese_search_result', actionGroup);

        popover.set_parent(label);
        popover.popup();
    }

    _openPronunciationContextMenu(x, y) {
        const builder = Gtk.Builder.new_from_resource(
            '/se/jadahl/LanguageAssistant/chinese-menus.ui');
        const popover = Gtk.PopoverMenu.new_from_model(
            builder.get_object('pronunciation_menu'));

        const encoder = new TextEncoder();

        const actionGroup = new Gio.SimpleActionGroup();

        let action;

        action = new Gio.SimpleAction({ name: 'copy-pinyin'});
        action.connect('activate', app => {
            const display = Gdk.Display.get_default();
            const clipboard = display.get_clipboard();
            const text =
                this._pronunciation.format(PronunciationVariant.HANYU_PINYIN);
            const contentProvider =
                Gdk.ContentProvider.new_for_bytes(
                    'text/plain;charset=utf-8', encoder.encode(text));
            clipboard.set_content(contentProvider);
        });
        actionGroup.insert(action);

        action = new Gio.SimpleAction({ name: 'copy-bopomofo'});
        action.connect('activate', app => {
            const display = Gdk.Display.get_default();
            const clipboard = display.get_clipboard();
            const text =
                this._pronunciation.format(PronunciationVariant.BOPOMOFO);
            const contentProvider =
                Gdk.ContentProvider.new_for_bytes(
                    'text/plain;charset=utf-8', encoder.encode(text));
            clipboard.set_content(contentProvider);
        });
        actionGroup.insert(action);
        popover.insert_action_group('pronunciation', actionGroup);

        popover.set_parent(this._pinyin);
        popover.popup();
    }

    maybeCreateHeader() {
        if (this._needsHeader) {
            let label = Gtk.Label.new(this._word.label);
            label.add_css_class('word-header');
            label.xalign = 0.0;
            return label;
        } else {
            return null;
        }
    }
});

class Separator {
};

var Chinese = class Chinese extends Language {
    constructor(window, readyCallback) {
        super(_('Chinese'));

        this._dictionary = new Cedict(readyCallback);

        this._window = window;

        const provider = Gtk.CssProvider.new();
        provider.load_from_resource(
            '/se/jadahl/LanguageAssistant/chinese.css');
        const display = Gdk.Display.get_default();
        Gtk.StyleContext.add_provider_for_display(
            display,
            provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        this._settings =
            Gio.Settings.new('se.jadahl.LanguageAssistant.chinese');
        const generalSettings =
            Gio.Settings.new('se.jadahl.LanguageAssistant');
        this._actionGroup = new Gio.SimpleActionGroup();
        let action = this._settings.create_action('search-method');
        action.connect('notify::state', () => this._window.invalidateSearch());
        this._actionGroup.add_action(action);
        action = generalSettings.create_action('presentation-mode');
        action.connect('notify::state', () => this._window.invalidateSearch());
        this._actionGroup.add_action(action);

        this._updateZoom();
        this._window.connect('zoom-changed', this._updateZoom.bind(this));
    }

    _updateZoom() {
        const display = Gdk.Display.get_default();

        if (this._zoomProvider) {
            Gtk.StyleContext.remove_provider_for_display(
                display, this._zoomProvider);
        }

        const provider = new Gtk.CssProvider();
        const scaleDiff = this._window.getScaleDiff();
        provider.load_from_data(
            `.chinese-pinyin {\n
               font-size: ${16 + scaleDiff}px;\n
             }\n
             \n
             .chinese-word {\n
               font-size: ${20 + scaleDiff}px;\n
             }\n
             \n
             .word-header {\n
               font-size: ${16 + scaleDiff}px;\n
             }\n
             \n
             .chinese-definition {\n
               font-size: ${16 + scaleDiff}px;\n
             }`);
        Gtk.StyleContext.add_provider_for_display(display, provider, 600);

        this._zoomProvider = provider;
    }

    takeOneWord(sentence) {
        let groups = sentance.trim().split(' ', limit=1);
        let firstGroup = null;

        for (let i = 0; i < groups.length; i++) {
            if (groups[i].length === 0)
                continue;
        }

        if (groups[0].length == 0)
           return [null, ''];
    }

    _processResults(results) {
        let widgets = [];
        let needsHeader = true;
        let index = 0;

        for (let result of results) {
            if (result instanceof Separator) {
                needsHeader = true;
                index++;
            } else {
                let pronunciation;
                let region = this._settings.get_enum('pronunciation-region');
                for (let pinyin of result.get_pinyins()) {
                    if (region === PronunciationRegion.TAIWAN &&
                        pinyin.variant === 'Taiwan')
                        pronunciation = new Pronunciation(pinyin.pinyin);
                    else if (!pronunciation &&
                             pinyin.variant === 'default')
                        pronunciation = new Pronunciation(pinyin.pinyin);
                }
                let widget = new ChineseResultEntry(
                    result.traditional,
                    pronunciation,
                    result.definitions,
                    needsHeader);
                if (result._first && result._last) {
                    widget.add_css_class('span-first');
                    widget.add_css_class('span-last');
                } else if (result._first) {
                    widget.add_css_class('span-first');
                } else if (result._last) {
                    widget.add_css_class('span-last');
                } else {
                    widget.add_css_class('span-middle');
                }

                const isEven = index % 2 === 0;
                if (isEven)
                    widget.add_css_class('result-even');
                else
                    widget.add_css_class('result-odd');

                widgets.push(widget);

                needsHeader = false;
            }
        }
        return widgets;
    }

    async _searchChineseWords(part) {
        let words = [];

        const rules = CedictSearchRule.BOTH;
        const fields =
            CedictSearchField.TRADITIONAL |
            CedictSearchField.SIMPLIFIED;

        let lastResult = null;
        let end = 1;
        while (true) {
            let word = [...part].slice(0, end).join('');
            if (word.length === 0)
                break;

            const result =
                await this._dictionary.search(word, rules, fields);

            if (result.length === 0 && end === 1) {
                part = [...part].slice(end, part.length).join('');
                end = 1;
            } else if (result.length === 0) {
                if (lastResult)
                    words.push(lastResult);
                part = [...part].slice(end - 1, part.length).join('');
                end = 1;
            } else if (end == [...part].length) {
                words.push(result);
                break;
            } else {
                lastResult = result;
                end++;
            }
        }

        return words;
    }

    async _searchPinyin(text) {
        const rules = CedictSearchRule.BOTH;
        const fields = CedictSearchField.PINYIN;
        let words = [];

        return [await this._dictionary.search(text, rules, fields)];
    }

    async _searchDefinition(text) {
        const rulesExact = CedictSearchRule.BOTH;
        const rulesBegin = CedictSearchRule.BEGIN;
        const rulesNone = CedictSearchRule.NONE;
        const fields = CedictSearchField.DEFINITION;

        const term = text.toLowerCase();

        const exact = await this._dictionary.search(term, rulesExact, fields);
        if (exact.length > 0)
          return [exact];

        const begin = await this._dictionary.search(term, rulesBegin, fields);
        if (begin.length > 0)
          return [begin];

        return [await this._dictionary.search(term, rulesNone, fields)];
    }

    async _searchPart(part) {
            const hasHan = /\p{Script=Han}/u;
            const isLatin = /\p{Script=Latin}/u;

            if (part.match(hasHan)) {
                return await this._searchChineseWords(part);
            } else if (part.match(isLatin)) {
                let result= await this._searchPinyin(part);
                if (result[0].length > 0)
                    return result;

                return await this._searchDefinition(part);
            }
    }

    async _searchWordByWord(text) {
        let sentence = new Sentence(text);
        let words = [];
        for (let part of sentence.parts) {
            const result = await this._searchPart(part);
            words = words.concat(result);
        }

        let results = [];
        for (let i = 0; i < words.length; i++) {
            for (let j = 0; j < words[i].length; j++) {
                words[i][j]._first = j === 0;
                words[i][j]._last = j === words[i].length - 1;
            }
            results = results.concat(words[i]);
            if (i < words.length - 1)
                results.push(new Separator());
        }

        return results;
    }

    async _searchFreeText(text) {
        const rules = CedictSearchRule.NONE;
        const fields = CedictSearchField.TRADITIONAL;
        let result = await this._dictionary.search(text, rules, fields);
        return result;
    }

    search(text, callback) {
        let searchMethod = this._settings.get_enum('search-method');
        const processResult = result => {
            const widgets = this._processResults(result);
            callback(widgets);
        };
        switch (searchMethod) {
            case SearchMethod.WORD_BY_WORD:
                this._searchWordByWord(text).then(processResult)
                    .catch(forwardError);
                break;
            case SearchMethod.FREE_TEXT:
                this._searchFreeText(text).then(processResult)
                    .catch(forwardError);
                break;
        }
    }

    getSearchOptionsPopover() {
        let builder = Gtk.Builder.new_from_resource(
            '/se/jadahl/LanguageAssistant/chinese-search-options.ui');
        let popover = Gtk.PopoverMenu.new_from_model(
            builder.get_object('chinese_search_options'));
        popover.insert_action_group('chinese', this._actionGroup);
        return popover;
    }
};
