/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Gdk } = imports.gi;

var SpanLayout = GObject.registerClass({
    GTypeName: 'SpanLayout',
}, class SpanLayout extends Gtk.LayoutManager {
    _calculateAllocations(widget, width, height) {
        const sizes = widget.getChildren().map((child) => {
            const [ , natWidth, , ] =
                child.measure(Gtk.Orientation.HORIZONTAL, -1);
            const [ , natHeight, , ] =
                child.measure(Gtk.Orientation.VERTICAL, natWidth);
            return [child, natWidth, natHeight];
        });

        const rows = [];
        let x = 0;
        for (const size of sizes) {
            const [child, natWidth, natHeight] = size;

            if (x === 0 || x + natWidth > width) {
                rows.push([size]);
                x = natWidth;
            } else {
                rows[rows.length - 1].push(size);
                x += natWidth;
            }
        }

        let allocations = [];
        let y = 0;
        for (const row of rows) {
            const maxHeight = row.reduce((prev, next) => {
                const prevHeight = prev;
                const [, , nextHeight] = next;
                return Math.max(prevHeight, nextHeight)
            }, 0);

            let x = 0;
            for (const size of row) {
                let [child, width, height] = size;
                let allocation = new Gdk.Rectangle({
                    x: x,
                    y: y,
                    width: width,
                    height: maxHeight,
                });
                allocations.push([child, allocation]);
                x += allocation.width;
            }
            y += maxHeight;
        }

        return allocations;
    }

    vfunc_measure(widget, orientation, forSize) {
        let allocations;
        if (orientation === Gtk.Orientation.VERTICAL) {
            if (widget.get_allocated_width() > 0 && forSize < 0)
                forSize = widget.get_allocated_width();
            allocations = this._calculateAllocations(widget, forSize, -1);
        } else if (orientation === Gtk.Orientation.HORIZONTAL) {
            allocations = this._calculateAllocations(widget, -1, forSize);
        }

        const [[absWidth, absHeight], [maxWidth, maxHeight]] =
            allocations.reduce((prev, next) => {
                const [
                    [prevAbsWidth, prevAbsHeight],
                    [prevMaxWidth, prevMaxHeight],
                ] = prev;
                const [, allocation] = next;
                return [
                    [
                        Math.max(prevAbsWidth,
                            allocation.x + allocation.width),
                        Math.max(prevAbsHeight,
                            allocation.y + allocation.height),
                    ],
                    [
                        Math.max(prevMaxWidth, allocation.width),
                        Math.max(prevMaxHeight, allocation.height),
                    ]
                ];
            }, [[0,0], [0,0]]);

        if (orientation === Gtk.Orientation.VERTICAL)
            return [absHeight, absHeight, -1, -1];
        else if (orientation === Gtk.Orientation.HORIZONTAL)
            return [maxWidth, absWidth, -1, -1];
    }

    vfunc_allocate(widget, width, height, baseline) {
        widget.queue_resize();
        const allocations = this._calculateAllocations(widget, width, height);
        for (const [child, allocation] of allocations)
            child.size_allocate(allocation, -1);
    }
});
