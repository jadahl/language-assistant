/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk } = imports.gi;

const { SpanLayout } = imports.spanLayout;

var SpanBox = GObject.registerClass({
    GTypeName: 'Span',
}, class Span extends Gtk.Widget {
    _init() {
        super._init();

        this.set_layout_manager(new SpanLayout());
        this._children = [];
        this.add_css_class('span-box');
    }

    addChild(widget) {
        this._children.push(widget);
        widget.add_css_class('span-child');
        widget.set_parent(this);
    }

    removeChild(widget) {
      const i = this._children.findIndex(widget);
      if (i === -1)
          return;
      widget.remove_css_class('span-child');
      this._children[i].set_parent(null);
      this._children.splice(i, 1);
    }

    getChildren() {
        return this._children;
    }

    clear() {
        for (const child of this._children)
            child.unparent();
        this._children = [];
    }
});
