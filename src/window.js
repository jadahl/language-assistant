/*
 * Copyright 2021 Jonas Ådahl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gio, Gtk, Adw, } = imports.gi;

const { Chinese } = imports.chinese;
const { SpanBox } = imports.spanBox;
const { PresentationMode } = imports.types;

var SearchOptionsButton = GObject.registerClass({
    GTypeName: 'SearchOptionsButton',
}, class SearchOptionsButton extends Gtk.Image {
    _init() {
        super._init({
            icon_name: 'view-more-symbolic',
        });

        this.add_css_class('search-options');

        let gesture = new Gtk.GestureClick();
        gesture.connect('pressed',
            gesture => gesture.set_state(Gtk.EventSequenceState.CLAIMED));
        gesture.connect('released', this._openSearchOptions.bind(this));
        this.add_controller(gesture);
    }

    setLanguage(language) {
        this._language = language;
        delete this._optionsPopover;
    }

    _openSearchOptions() {
        if (!this._optionsPopover) {
            this._optionsPopover = this._language.getSearchOptionsPopover();
            this._optionsPopover.set_parent(this);
        }
        this._optionsPopover.popup();
    }

    vfunc_unmap() {
        super.vfunc_unmap();
        if (this._optionsPopover) {
            this._optionsPopover.unparent();
            delete this._optionsPopover;
        }
    }

    vfunc_size_allocate(width, height, baseline) {
        super.vfunc_size_allocate(width, height, baseline);
        if (this._optionsPopover)
            this._optionsPopover.present();
    }
});

var LanguageAssistantWindow = GObject.registerClass({
    GTypeName: 'LanguageAssistantWindow',
    Template: 'resource:///se/jadahl/LanguageAssistant/window.ui',
    InternalChildren: [
        'main_entry',
        'result_list',
        'result',
    ],
    Signals: {
        'zoom-changed': {},
    },
}, class LanguageAssistantWindow extends Adw.ApplicationWindow {
    _init(application) {
        super._init({ application });

        this._initSettings();
        this._initLanguages();

        let actionGroup = new Gio.SimpleActionGroup();
        this.insert_action_group('win', actionGroup);

        let action = new Gio.SimpleAction({
            name: 'select-main-entry',
        });
        action.connect('activate', this._selectMainEntry.bind(this));
        actionGroup.add_action(action);

        this._optionsButton = new SearchOptionsButton();
        this._optionsButton.set_parent(this._main_entry);
        this._optionsButton.setLanguage(this._currentLanguage);

        const lastSearchTerm = this._settings.get_string('last-search-term');
        this._main_entry.set_text(lastSearchTerm);

        const focusController = new Gtk.EventControllerFocus();
        focusController.connect('enter', this._onMainEntryEnter.bind(this));
        this._main_entry.add_controller(focusController);
        this._hasInitialFocus = false;

        this._main_entry.connect('search-changed',
            this._mainEntryChanged.bind(this));

        this._result_spans = new SpanBox();
        this._result.add_child(this._result_spans);

        this._result_list.set_header_func((row, before) => {
            let header = row.get_child().maybeCreateHeader();
            row.set_header(header);
        });

        this._syncPresentationMode();

        this.connect('close-request', this._clearResultSpans.bind(this));
    }

    vfunc_unmap() {
        super.vfunc_unmap();
        if (this._optionsButton) {
            this._optionsButton.unparent();
            delete this._optionsButton;
        }
    }

    _onMainEntryEnter() {
        if (this._hasInitialFocus)
            return;

        this._hasInitialFocus = true;
        this._selectMainEntry();
    }

    _selectMainEntry() {
        this._main_entry.select_region(0, -1);
        this.focus_widget = this._main_entry;
    }

    _clearResultList(widget) {
        while (true) {
            let lastChild = this._result_list.get_last_child();
            if (!lastChild)
                break;
            this._result_list.remove(lastChild);
        }
    }

    _clearResultSpans() {
        this._result_spans.clear();
    }

    _search() {
        if (!this._languageReady)
            return;

        const text = this._main_entry.get_text();

        if (text === '')
            return;

        this._currentLanguage.search(text, (widgets) => {
            this._clearResultList();
            this._clearResultSpans();

            const presentationMode = this._settings.get_enum('presentation-mode');

            for (const widget of widgets)
                widget.setPresentationMode(presentationMode);

            switch (presentationMode) {
                case PresentationMode.LIST:
                    for (const widget of widgets)
                        this._result_list.append(widget);
                    break;
                case PresentationMode.WORD_BY_WORD:
                    for (const widget of widgets)
                        this._result_spans.addChild(widget);
                    break;
            }
        });
    }

    _mainEntryChanged() {
        this._search();
        const entryText = this._main_entry.get_text();
        this._settings.set_string('last-search-term', entryText)
    }

    _syncPresentationMode() {
        const presentationMode = this._settings.get_enum('presentation-mode');
        switch (presentationMode) {
            case PresentationMode.LIST:
                this._result.set_visible_child(this._result_list);
                break;
            case PresentationMode.WORD_BY_WORD:
                this._result.set_visible_child(this._result_spans);
                break;
        }
    }

    _syncZoom() {
        this.emit('zoom-changed');
    }

    _initSettings() {
        this._settings =
            Gio.Settings.new('se.jadahl.LanguageAssistant');
        this._settings.connect('changed::presentation-mode',
            this._syncPresentationMode.bind(this));
        this._settings.connect('changed::zoom',
            this._syncZoom.bind(this));
    }

    _onLanguageReady() {
        this._languageReady = true;
        this._search();
    }

    _initLanguages() {
        this._languageReady = false;
        this._languages = [
            new Chinese(this, this._onLanguageReady.bind(this)),
        ];
        this._currentLanguage = this._languages[0];
    }

    invalidateSearch() {
        this._search();
    }

    zoom(diff) {
        const zoom = this._settings.get_int('zoom');
        this._settings.set_int('zoom', zoom + diff);
    }

    getScaleDiff() {
        return this._settings.get_int('zoom');
    }
});
